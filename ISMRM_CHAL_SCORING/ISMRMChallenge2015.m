% Jeff Tsao, Alexey Samsonov Apr-June, 2012
% Samuel A. Hurley (DB Connection Parts, 1-Jun-2012)
%
%   06/23/2013 - modified for ISMRM 2013 Challenge by AS.
%

%%
% Setup MySQL Connector/J Interface for Matlab
clear all

isFullReset=1;  %  Full reset overwrites score_history.log and error.log files; otherwise, it appends them.

testpath = '/Users/guille/code/ismrmrfpd/ISMRM_CHAL_SCORING/';
%oldpath = '/export/home/ismrm/public_html/2013';

addpath(fullfile(testpath, 'src'));
javaaddpath(fullfile(testpath,'src/+edu/+stanford/+covert/+db'));
javaaddpath(fullfile(testpath,'lib/mysql-connector-java-5.1.6/mysql-connector-java-5.1.6-bin.jar');

import edu.stanford.covert.db.*;
UPLOADDIR=fullfile(testpath,'UPLOADS');
% UPLOADDIR='/data/data_ideal2/samsonov/DATA/CHALLENGE/UPLOADS';
REFDATADIR=fullfile(testpath,'REFDATA');
STOREDIR=fullfile(testpath,'UPLOADS');

% -- load reference datasets
tmp=load([REFDATADIR '/refdata.mat']);
MASKS=tmp.MASKS;
REFCASES=tmp.REFCASES;
REFCASES_ZF=tmp.REFCASES_ZF;

clear tmp
scoreMode=1;  % 1 - julia, 2 - kevin, 3 - L2
nCases=numel(REFCASES);

% -- cutoffs for score 0
STH=ones(nCases,1);

for ii=1:nCases
    %     MASKS{ii}=abs(ksmooth_nonsq(std(REFCASES{ii},[],3),128)).*MASKS{ii};
    MASKS{ii}=filter2(gausswin(15)*gausswin(15)',abs(std(REFCASES{ii},[],3)).*MASKS{ii});
end

% WAG: Calculate baseline scores; get_score will use these to calculate
% scores as ratios
for ii=1:nCases
    [~, S0(ii)]=get_score2013(REFCASES{ii},REFCASES{ii}*0,MASKS{ii},scoreMode,ones(nCases,1));
    fprintf(1,'Case %d, S0=%f\n',ii,S0(ii));
end

isScoreTableUpdated=0; % Flag to indicate score table on ISMRM side has been updated
isResetUpdateTime=1;

if isFullReset
    fm='w';
    isResetUpdateTime=1;
else
    fm='a';
end

fidSL=fopen([REFDATADIR '/scores_history.log'],fm);
fidErr=fopen([REFDATADIR '/error.log'],fm);

checkinterval = 30;

while 1
    try
        db = MySQLDatabase('www.ismrm.org', 'deltakhk_ismrm_recon', 'deltakhk_ismrm', 'ismrm_recon_2012');
        
        % ISMRM table update
        if ~isScoreTableUpdated
            db.prepareStatement('SELECT entity_id,field_score_value FROM field_data_field_score');
            resultCS = db.query();
            for CN=1:nCases
                db.prepareStatement(['SELECT entity_id,field_score_' num2str(CN, '%02.0f') '_value FROM field_data_field_score_' num2str(CN, '%02.0f')]);
                result = db.query();
                for jj=1:numel(resultCS.entity_id);
                    Scores(result.entity_id(jj),CN)= str2num(eval(['result.field_score_' num2str(CN, '%02.0f') '_value{jj}']));
                    if CN==1
                        Scores(resultCS.entity_id(jj),nCases+1) = str2num(resultCS.field_score_value{jj});
                    end
                end
            end
            
            UpdateTime=zeros(size(Scores,1),6);
            
            db.prepareStatement(['SELECT entity_id,field_update_time_value FROM field_data_field_update_time']);
            UT=db.query();
            for jj=1:numel(UT.entity_id);
                tmp=UT.field_update_time_value{jj};
                if numel(tmp)>4
                    UpdateTime(UT.entity_id(jj), 1:6)=datevec(tmp(1:end-4));
                end
            end
            isScoreTableUpdated=1;
        end
        
        % -- get user id numbers and e-mail addresses
        % -- create database connection
        db.prepareStatement('SELECT uid,mail FROM users');
        dbUserIDs = db.query();
        
        % -- Cleanup
        db.close();
        
        uid=[];
        for ii = 1:length(dbUserIDs.uid)
            uid(ii) = dbUserIDs.uid(ii);
            eml{ii} = dbUserIDs.mail(ii,:);
        end
        
        % -- get file names
        names = dirf([UPLOADDIR '/*.*.*.*.*.*.*.raw']);
        TS=zeros(numel(names),8);
        for ii=1:numel(names)
            [TS(ii,1), TS(ii,2), TS(ii,3), TS(ii,4), TS(ii,5), TS(ii,6),TS(ii,7),TS(ii,8),~]=strread(names{ii},'%d%d%d%d%d%d%d%d%s','delimiter','.');
        end
        
        tID =TS(:,1);
        cID =TS(:,2);
        dt  =TS(:,3:end);
        
        % -- identify teams
        uid=setdiff(uid,[0 1]);
        teamIDs = uid;
        % Reports=cell(max(teamIDs),1); % Reset reports
        
        if size(Scores,1)<max(teamIDs)
            Scores(max(teamIDs),:)=0;
            UpdateTime(max(teamIDs),:)=0;
        end
        
        Scores0=Scores;
        if isResetUpdateTime
            UpdateTime(:)=0;
            isResetUpdateTime=0;
        end
        UpdateTime0=UpdateTime;
        
        % Actual Scoring/Book Keeping
        for ii=1:numel(teamIDs)  % for each participating team
            teamID=teamIDs(ii);
            UpdateTimeTeam=zeros(nCases,6);
            for jj=1:nCases
                ind = find(tID==teamID & cID==jj);
                if ~isempty(ind)
                    [~, iS]=sortrows(dt(ind,:));
                    ind_last=ind(iS(end));
                    % Purge out datasets that are 7 day or more old; keep the
                    % latest one
                    time_diff=now-datenum(dt(ind,:));
                    %                     indOUT = find(time_diff>7 & ind ~=ind_last);
                    indOUT = find(ind ~=ind_last);
                    
                    for kk=1:numel(indOUT)
                        delete([UPLOADDIR '/' names{ind(indOUT(kk))}]);
                        % Update log?
                    end
                    
                    % ****** Score the latest case for the given team *****                 
                    if  datenum(dt(ind_last,:))>datenum(UpdateTime(teamID, :))
                        UpdateTimeTeam(jj, :)=dt(ind_last,:);
                        fid = fopen([UPLOADDIR '/' names{ind_last}],'r','ieee-le');
                        if fid>=1
                            refmask=MASKS{jj};
                            refdata=REFCASES{jj};
                            ims = reshape(fread(fid,inf,'float'),size(refdata));                          
                            
                            S = get_score2013(ims,refdata,refmask,scoreMode,S0(jj),STH(jj));                            
                            
                            fprintf(1,'Team %d, Case %d, Score %f\n',teamID,jj,S);
                            fprintf([UPLOADDIR '/' names{ind_last} '\n']);
                            Scores(teamID,jj)=S;
                            fclose(fid);
                        else
                            % Update log, generate email?
                            % Do nothing then
                        end
                    end
                    % ****************************************************
                    
                else
                    % No cases were submitted yet
                end
            end
            [~, iS]=sortrows(UpdateTimeTeam);
            if  datenum(UpdateTimeTeam(iS(end),:))>datenum(UpdateTime(teamID, :))
                UpdateTime(teamID, :)=UpdateTimeTeam(iS(end),:);
            end
        end
        
        %  Update Database
        CS=floor(mean(Scores(teamIDs,1:nCases),2));
        Scores(teamIDs,nCases+1)=CS;
        if sum(datenum(UpdateTime(teamIDs, :))-datenum(UpdateTime0(teamIDs, :))>0)
            msg=[];
            db = MySQLDatabase('www.ismrm.org', 'deltakhk_ismrm_recon', 'deltakhk_ismrm', 'ismrm_recon_2012');
            for kk=1:numel(CS)
                teamID=teamIDs(kk);
                % Update individual scores
                for CN=1:nCases
                    if Scores(teamID,CN)~=Scores0(teamID,CN)
                        db.prepareStatement(['UPDATE field_data_field_score_' num2str(CN, '%02.0f') ' SET field_score_' num2str(CN, '%02.0f') '_value = "' num2str(floor(Scores(teamID,CN)), '%05.0f') '" WHERE entity_id = ' num2str(teamID)]);
                        db.query();
                        db.prepareStatement(['UPDATE field_revision_field_score_' num2str(CN, '%02.0f') ' SET field_score_' num2str(CN, '%02.0f') '_value = "' num2str(floor(Scores(teamID,CN)), '%05.0f') '" WHERE entity_id = ' num2str(teamID)]);
                        db.query();
                        % Update Score Log
                        fprintf(fidSL, 'TeamID: %d Case: %d Time: %s Score: %d\n', teamID, CN, datestr(now), Scores(teamID,CN));
                    end
                end
                
                %  Update total score
                if CS(kk)~=Scores0(teamID,nCases+1)
                    % Write new score
                    db.prepareStatement(['UPDATE field_data_field_score SET field_score_value = "' num2str(CS(kk), '%05.0f') '" WHERE entity_id = ' num2str(teamID)]);
                    db.query();
                    db.prepareStatement(['UPDATE field_revision_field_score SET field_score_value = "' num2str(CS(kk), '%05.0f') '" WHERE entity_id = ' num2str(teamID)]);
                    db.query();
                end
                
                % Last updated time/date
                if  datenum(UpdateTime(teamID, :))>datenum(UpdateTime0(teamID, :))
                    db.prepareStatement(['UPDATE field_data_field_update_time SET field_update_time_value = "' sprintf(' %s CST',datestr(UpdateTime(teamID, :))) '" WHERE entity_id = ' num2str(teamID)]);
                    db.query();
                    db.prepareStatement(['UPDATE field_revision_field_update_time SET field_update_time_value = "' sprintf(' %s CST',datestr(UpdateTime(teamID, :))) '" WHERE entity_id = ' num2str(teamID)]);
                    db.query();
                end
            end
            db.close();
            % Cleanup
            !curl http://www.ismrm.org/challenge/clear_cache.php
            fprintf(1,'Database updated\n');
        end
        
        % Add header and send out reports by email if any in Reports is non-empty
        % *** Sam?
        % Update log
        
        save([REFDATADIR '/scores1.mat'],'Scores');
    catch MErr
        %     email error message
        disp('Opps')
        isScoreTableUpdated=0;
        fprintf(1,'%s,  %s, Line %d\n', MErr.identifier, MErr.message, MErr.stack.line);
        fprintf(fidErr,'Time:  %s:  %s,  %s, Line %d\n', datestr(now), MErr.identifier, MErr.message, MErr.stack.line);
    end    
    pause(checkinterval);
end

fclose(fidSL);

return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Clean up code  (To be run in extreme cases when database has to be reset!)

db = MySQLDatabase('www.ismrm.org', 'deltakhk_ismrm_recon', 'deltakhk_ismrm', 'ismrm_recon_2012');
db.prepareStatement('SELECT entity_id,field_score_value FROM field_data_field_score');
result = db.query();
teamIDs=result.entity_id;
for kk=1:numel(teamIDs)
    %  Cleaning UpdateTime
    db.prepareStatement(['UPDATE field_data_field_update_time SET field_update_time_value = "  " WHERE entity_id = ' num2str(teamIDs(kk))]);
    db.query();
    db.prepareStatement(['UPDATE field_revision_field_update_time SET field_update_time_value = "  " WHERE entity_id = ' num2str(teamIDs(kk))]);
    db.query();
    
    % Cleaning Individual Scores
    for jj=1:nCases
        db.prepareStatement(['UPDATE field_data_field_score_' num2str(jj, '%02.0f') ' SET field_score_' num2str(jj, '%02.0f') '_value = "00000" WHERE entity_id = ' num2str(teamIDs(kk))]);
        db.query();
        db.prepareStatement(['UPDATE field_revision_field_score_' num2str(jj, '%02.0f') ' SET field_score_' num2str(jj, '%02.0f') '_value = "00000" WHERE entity_id = ' num2str(teamIDs(kk))]);
        db.query();
    end
    
    % Cleaning Total Score
    db.prepareStatement(['UPDATE field_data_field_score SET field_score_value = "00000" WHERE entity_id = ' num2str(teamIDs(kk))]);
    db.query();
    db.prepareStatement(['UPDATE field_revision_field_score SET field_score_value = "00000" WHERE entity_id = ' num2str(teamIDs(kk))]);
    db.query();
end
db.close();
!curl http://www.ismrm.org/challenge/clear_cache.php


