function [S S1]=get_score2013(ims,refdata,refmask, mode, S0, Sthresh)

Nfr=size(ims,3);
if ~check_var('mode')
    mode=1;
end

if ~check_var('Sthresh')
    Sthresh=1;
end

switch mode
    case 1
        %         for ii=1:Nfr
        S1=besov_weighted2(refdata-ims,4,1,0.75,refmask);
        if ~check_var('S0')
            S0=besov_weighted2(refdata,4,1,0.75,refmask);
        end
        S2=S1/S0;
    case 2
        S1=w_besovAS(refdata, ims, refmask);
        if ~check_var('S0')
            S0=w_besovAS(refdata, 0, refmask);
        end
        S2=S1./S0;
    case 3
        mask=repmat(refmask,[1 1 Nfr]);
        S1=norm(mask(:).*(refdata(:)-ims(:)));
        S0=norm(mask(:).*refdata(:));
        S2=S1/S0;
end

S3=S2;
if S3 > Sthresh
    S3=Sthresh;
end

S=floor((Sthresh-S3)/Sthresh*10000);

if S==0
    S=0;
end

end

