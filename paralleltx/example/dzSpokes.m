function [rf,g,dt,m,kx,ky] = dzSpokes(maps,probp)

% Spokes parallel transmit RF pulse design example
% Written for 2014 ISMRM Nuts and Bolts Course
% Adapted for ISMRM RF Pulse Design Challenge
% Will Grissom, Vanderbilt University, 2015
% will.grissom@vanderbilt.edu
%
%
%
%
%

[dim(1),dim(2),Nc] = size(maps.b1); % get b1 map matrix size and # tx channels
maps.b1 = reshape(maps.b1,[prod(dim) Nc]); % vectorize the b1 maps

%% Step 1: set up design grid and (kx,ky) search grid
x = maps.xy(:,:,1);
y = maps.xy(:,:,2);
kmax = 1/probp.deltaxmin; % /cm, max spatial freq of traj
[kxs,kys] = meshgrid(-kmax/2:1/max(maps.fov):kmax/2-1/max(maps.fov)); % greedy kx-ky grid
kxs = kxs(:);kys = kys(:); % vectorize grid
dc = find((kxs == 0) & (kys == 0)); % find DC point
if ~isempty(dc)
    kxs = kxs([1:dc-1 dc+1:end]);kys = kys([1:dc-1 dc+1:end]); % remove DC point
end


%% Step 2: design the weights
kx = 0; % initial kx/ky location is DC
ky = 0;
if isfield(probp,'phsinit')
    phs = probp.phsinit(maps.mask);
else
    phs = zeros(sum(maps.mask(:)),1);
end
m = {};
Afi = inv(maps.b1(maps.mask,:)'*maps.b1(maps.mask,:)); % precalc inverse of greedy matrix
for ii = 1:probp.Nspokes

    % build system matrix
    A = exp(1i*2*pi*(x(maps.mask)*kx(:)' + y(maps.mask)*ky(:)')); % Fourier matrix
    Afull = zeros(sum(maps.mask(:)),ii*Nc);
    for jj = 1:Nc
        % multiply B1 maps and Fourier kernel - bsxfun does this quickly
        Afull(:,(jj-1)*ii+1:jj*ii) = bsxfun(@times,maps.b1(maps.mask,jj),A);
    end

    % calculate magnitude least-squares-optimal RF weights
    wfull = inv(Afull'*Afull + probp.beta*eye(ii*Nc))*(Afull'*exp(1i*phs));
    err = Afull*wfull - exp(1i*phs);
    cost = real(err'*err + probp.beta*wfull'*wfull);
    costold = 10*cost; % to get loop going
    m = zeros(dim(1),dim(2));
    while abs(cost-costold) > 0.0001*costold
        costold = cost;
        phs = angle(Afull*wfull); % set phase to current excited phase
        wfull = (Afull'*Afull + probp.beta*eye(ii*Nc))\(Afull'*exp(1i*phs));
        err = Afull*wfull - exp(1i*phs);
        cost = real(err'*err + probp.beta*wfull'*wfull);
        m = zeros(dim(1),dim(2));
        m(maps.mask) = Afull*wfull;
    end

    fprintf('Number of spokes: %d. Normalized flip angle variance: %0.2d\n',ii,var(abs(Afull*wfull)));

    % add a spoke using greedy method
    if ii < probp.Nspokes
        r = exp(1i*phs) - Afull*wfull;
        rfnorm = [];
        for jj = 1:length(kxs)
            Afull = bsxfun(@times,maps.b1(maps.mask,:),exp(1i*2*pi*(x(maps.mask)*kxs(jj)' + y(maps.mask)*kys(jj))));
            rfnorm(jj) = norm(Afi*(Afull'*r));
        end
        [~,ind] = max(rfnorm);
        % alternate which end of the pulse we add the spoke to
        if rem(ii + 1,2)
            kx = [kx;kxs(ind)];ky = [ky;kys(ind)];
        else
            kx = [kxs(ind);kx];ky = [kys(ind);ky];
        end
        % remove selected point from search grid
        kxs = kxs([1:ind-1 ind+1:end]);
        kys = kys([1:ind-1 ind+1:end]);
    end

end


%% Step 3: build the full waveforms
area = probp.tb/(probp.slthick/10)/4258; % slthick*kwidth = tbw; kwidth = gamma*area
[subgz,nramp] = domintrap(area,probp.maxg/10,probp.maxgslew*100,probp.dt); % min-duration trapezoid
subgz = subgz*10; % mT/m
if ~isempty(which('dzrf')) % check if Pauly SLR tool is available
    subrf = dzrf(128,probp.tb,'st'); % get small-tip slice-selective subpulse
    save subrf subrf;
else
    load subrf;
end
Nplat = length(subgz) - 2*nramp; % # time points on trap plateau
subrf = interp1((0:127)/128,subrf,(0:Nplat-1)/Nplat,'spline',0);
subrf = [zeros(nramp,1);subrf(:);zeros(nramp,1)];
subrf = subrf./sum(subrf);

% Build gradient waveforms
gxarea = diff([kx; 0])/4258; % no (-) sign since we don't time-flip k
gyarea = diff([ky; 0])/4258;

gx = [];gy = [];gz = [];
for ii = 1:probp.Nspokes
    gz = [gz;((-1)^(ii-1))*subgz(:)];
    gx = [gx;zeros(length(subgz),1)];
    if abs(gxarea(ii)) > 0
        gxblip = sign(gxarea(ii))*dotrap(abs(gxarea(ii)),probp.maxg/10,probp.maxgslew*100,probp.dt)*10;
        gx(end-length(gxblip)+1:end) = gxblip;
    end
    gy = [gy;zeros(length(subgz),1)];
    if abs(gyarea(ii)) > 0
        gyblip = sign(gyarea(ii))*dotrap(abs(gyarea(ii)),probp.maxg/10,probp.maxgslew*100,probp.dt)*10;
        gy(end-length(gyblip)+1:end) = gyblip;
    end
end

gzref = ((-1)^probp.Nspokes)*dotrap(probp.dt*sum(subgz)/2/10,probp.maxg/10,probp.maxgslew*100,probp.dt)'*10;
gz = [gz;gzref];
gx = [gx;zeros(size(gzref))];
gy = [gy;zeros(size(gzref))];
g = [gx(:) gy(:) gz(:)];

% Build full RF waveforms
rf = reshape(kron(wfull,subrf),[length(gz)-length(gzref) Nc]);
rf = [rf;zeros(length(gzref),Nc)];

% Scale to the target flip angle
rf = rf*(probp.flipAngle/180*pi)/(probp.dt*42.57*2*pi);

% if RF violates peak B1 constraint, adjust dt
if max(abs(rf(:))) > probp.maxb1
    dt = max(abs(rf(:)))/(0.99*probp.maxb1)*probp.dt;
    rf = rf*probp.dt/dt;
    g = g*probp.dt/dt;
else
    dt = probp.dt;
end

% if RF violates peak SAR, increase dt
if isfield(maps,'vop')
  nVOP = size(maps.vop,3); % # VOPs
  sarVOP = zeros(nVOP,size(rf,1));
  for ii = 1:size(rf,1)
    rft = rf(ii,:); % all coil's samples for this time point
    for jj = 1:nVOP
      sarVOP(jj,ii) = real(rft(:)'*(maps.vop(:,:,jj)*rft(:)));
    end
  end
  sarVOP = sum(sarVOP,2) * dt./probp.TR;
  sarVOPfrac = sarVOP./maps.maxSAR(:);
  if any(sarVOPfrac > 1) % if we are violating an SAR constraint
    dt = dt*max(sarVOPfrac);
    rf = rf./max(sarVOPfrac);
    g = g./max(sarVOPfrac);
  end
end

%% Plot the results
figure
subplot(212)
plot((0:length(gx)-1)*dt*1000,[gx(:) gy(:) gz(:)])
axis([0 length(gx)*dt*1000 -probp.maxg probp.maxg]);
xlabel 'ms',ylabel 'mT/m'
title 'Gradient Waveforms'
subplot(211)
plot((0:length(gx)-1)*dt*1000,real(rf))
ysc = max(abs(min(real(rf(:)))),abs(max(real(rf(:)))));
axis([0 length(gx)*dt*1000 -1.1*ysc 1.1*ysc]);
xlabel 'ms',ylabel 'a.u.'
title 'RF Waveforms (real part)'

figure;imagesc(abs(m)*probp.flipAngle);title '',axis off,axis image
colormap jet
