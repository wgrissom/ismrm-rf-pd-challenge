%% load maps
disp 'Getting evaluation parameters and loading maps...'

evalp = paralleltxparams_torso; % get evaluation parameter structure

% load source b1/df0 maps and tissue mask ('maps' structure)
% maps.b1: [Nx Ny Nz Nc] (uT), complex B1+ maps
% maps.df0: [Nx Ny Nz] (Hz), off-resonance map
% maps.mask: [Nx Ny Nz] (logical), tissue mask
% maps.fov: [3] (cm), fov in each dimension
% maps.vop: [Nc Nc Nvop], SAR VOPs
% maps.maxSAR: [Nvop], max SAR at each VOP
% maps.xyz  [Nx*Ny*Nz 3], spatial locations of maps
% This will also overwrite the evalp structure
load(evalp.evalmapsfname);

%% design a spokes pulse
disp 'Designing a spokes pulse...'

% select the center slice from the volume for spokes design
probp.Nspokes = 5;
probp.flipAngle = evalp.flipAngle;
probp.beta = 10^-1;
probp.slthick = evalp.slThick;
probp.tb = evalp.tb;
probp.dt = 4e-6; % s, dwell time
probp.maxg = evalp.maxg;
probp.maxgslew = evalp.maxgslew;
probp.deltaxmin = 10; % finest possible res of kx-ky spokes traj (cm)
probp.maxb1 = evalp.maxb1; % peak digital RF amplitude constraint
probp.TR = evalp.TR; % TR for SAR calculation
% get the center slice b1+/df0 maps for the design
slMapInd = round((evalp.slCent + maps.fov(3)/2)/maps.fov(3)*size(maps.b1,3));
spokesmaps.b1 = squeeze(maps.b1(:,:,slMapInd,:));
spokesmaps.mask = squeeze(maps.mask(:,:,slMapInd));
spokesmaps.fov = maps.fov(1:2);
spokesmaps.xy = reshape(evalp.xyz(:,1:2),[size(maps.mask) 2]); % design with same xy grid as evaluation, to ensure agreement
spokesmaps.xy = squeeze(spokesmaps.xy(:,:,1,:));
spokesmaps.vop = evalp.vop;
spokesmaps.maxSAR = evalp.maxSAR;
% get a quadrature initial target phase pattern
tmp = 0;
for ii = 1:evalp.Nc
    tmp = tmp + exp(1i*(ii-1)/evalp.Nc*2*pi)*spokesmaps.b1(:,:,ii);
end
probp.phsinit = angle(tmp);
[rf,g,dt,m,kx,ky] = dzSpokes(spokesmaps,probp);


%% evaluate the spokes pulse
disp 'Evaluating the spokes pulse...'

evalp.fName = 'spokes_eval';
evalp.genfig = true;
[isvalid,dur,errorcode] = pTxEval(rf,g,dt,maps,evalp);
