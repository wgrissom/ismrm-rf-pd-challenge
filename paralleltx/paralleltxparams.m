function evalp = paralleltxparams

% defines parameters for the parallel transmit pulse design challenge

%% scanner constants
evalp.maxg = 80; % mT/m
evalp.maxgslew = 200; % mT/m/ms
evalp.maxb1 = 1; % digital units
evalp.gamma = 2*pi*42.58; % (rad/s)/uT, proton gyro ratio

%% source B1/df0 maps
evalp.sourcemapsfname = 'brain_8ch_b1df0.mat'; % Name of the .mat file containing the original source b1/df0 maps.
% File is assumed to contain a 'maps' structure with fields:
%   maps.b1: [Nx Ny Nz Nc] (uT), complex B1+ maps
%   maps.df0: [Nx Ny Nz] (Hz), off-resonance map
%   maps.mask: [Nx Ny Nz] (logical), tissue mask
%   maps.fov: [3] (cm), fov in each dimension
load(evalp.sourcemapsfname);
evalp.Nc = size(maps.b1,4); % # Tx channels

%% target pattern specs
evalp.flipAngle = 30; % target flip angle (degrees)
evalp.slCent = 0; % slice position in z (cm)
evalp.maxInSliceErr = 10; % max in-slice flip angle error (degrees)
evalp.maxInSliceRMSE = 2; % max in-slice flip angle RMSE (degrees)
evalp.maxOutOfSliceErr = evalp.flipAngle*0.01; % max out-of-slice flip angle error (degrees)
evalp.tb = 4; % time-bandwidth product of slice profile
evalp.slThick = 2; % target slice thickness (mm)
evalp.dxyz = [maps.fov(1)/size(maps.b1,1)/2 maps.fov(2)/size(maps.b1,2)/2 ...
  (evalp.slThick/20)/10]; % resolution of target pattern for evaluation (cm)
  % Note: for NUFFT to maintain accuracy, in-plane grid size should be > 64 or so
evalp.maxPhsDev = 0.05; % radians, max amount of through-slice phase deviation from mean phase

%% evaluation switches
evalp.maxMatrixDim = 2^10; % max eval system matrix dimension in any direction
evalp.useNUFFT = true; % use NUFFT, or build explicit system matrix?
