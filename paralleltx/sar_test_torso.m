doCluster = 0;
fullCalc = 0;

load('spokes_eval','rf','dt','evalp');
%rf = rf./max(abs(rf(:)));

load EM_forISMRMchallenge_uniformSlices.mat
SAR_factor = EM_forISMRM_uniformSlices.SAR_factor;
Ex = EM_forISMRM_uniformSlices.Ex;
Ey = EM_forISMRM_uniformSlices.Ey;
Ez = EM_forISMRM_uniformSlices.Ez;
Sx = sqrt(0.5)*Ex.*repmat(sqrt(SAR_factor),[1 1 1 8]);
Sy = sqrt(0.5)*Ey.*repmat(sqrt(SAR_factor),[1 1 1 8]);
Sz = sqrt(0.5)*Ez.*repmat(sqrt(SAR_factor),[1 1 1 8]);

mask = sum(abs(Ex) + abs(Ey) + abs(Ez),4).*abs(SAR_factor) > 0;
Sx = permute(Sx,[4 1 2 3]);Sx = Sx(:,:).';Sx = Sx(mask,:);
Sy = permute(Sy,[4 1 2 3]);Sy = Sy(:,:).';Sy = Sy(mask,:);
Sz = permute(Sz,[4 1 2 3]);Sz = Sz(:,:).';Sz = Sz(mask,:);

Nc = 8;
Ns = size(Sx,1);
S = zeros(Nc,Nc,Ns);
for ii = 1:Ns
  S(:,:,ii) = Sx(ii,:)'*Sx(ii,:) + Sy(ii,:)'*Sy(ii,:) + Sz(ii,:)'*Sz(ii,:); 
end

if doCluster
  % Cluster them
  omInds = 1:Ns; % the pool of SAR matrices (set Omega in most texts)
  nVOP = 0;
  Sv = []; % VOPs
  nSv = []; % number of spatial loc per VOP
  epsPsd = 0.5;
  % first calculate all spectral norms in set Omega
  specNorms = zeros(Ns,1);
  for ii = 1:Ns
    specNorms(ii) = norm(S(:,:,ii),2);
  end
  while ~isempty(omInds)
    
    % find max spectral norm remaining in set omega
    [~,maxInd] = max(specNorms(omInds));
    
    % make the matrix with max spectral norm a VOP
    SvNew = S(:,:,omInds(maxInd));
    omInds = omInds([1:maxInd-1 maxInd+1:end]);
    
    % find all the matrices that are eps-psd with the VOP, remove them from the set Omega
    lam = 0*omInds;
    for ii = 1:length(omInds)
      lam(ii) = min(real(eig(SvNew-S(:,:,omInds(ii)))));
    end
    omInds = omInds(lam < -epsPsd); % only keep matrices that are not eps-psd
    
    nVOP = nVOP + 1;
    Sv(:,:,nVOP) = SvNew;
    nSv(nVOP) = 1+sum(lam >= -epsPsd); % number of points in this cluster (+1 for the VOP point itself)
    fprintf('Total VOPs: %d. Points remaining to be clustered: %d.\n',size(Sv,3),length(omInds));
    
  end
else
  load VOPs_epspt1.mat
end

%rf = randn(20,8) + 1i*randn(20,8);

% calculate average and max SAR using VOPs and input RF
sarVOP = zeros(nVOP,size(rf,1));
aveSARvop = 0;
Sall = mean(S,3);
for ii = 1:size(rf,1)
  rft = rf(ii,:);
  for jj = 1:nVOP
    sarVOP(jj,ii) = real(rft(:)'*(Sv(:,:,jj)*rft(:)));
  end
  aveSARvop = aveSARvop + real(rft(:)'*(Sall*rft(:)));
end
maxSARvop = max(sum(sarVOP,2));
maxSARvop = maxSARvop * dt./evalp.TR;
aveSARvop = aveSARvop * dt./evalp.TR;

if fullCalc
  % calculate average and max SAR using full fields
  sarAll = zeros(Ns,size(rf,1));
  for ii = 1:size(rf,1)
    ii
    rft = rf(ii,:);
    for jj = 1:Ns
      sarAll(jj,ii) = real(rft(:)'*(S(:,:,jj)*rft(:)));
    end
  end
  maxSARall = max(sum(sarAll,2));
  aveSARall = mean(sum(sarAll,2));
end

% test with an SAR matrix
%sind = [93 38 1];
%Nc = 8;
%Sxv = squeeze(Sx(sind(1),sind(2),sind(3),:));
%Syv = squeeze(Sy(sind(1),sind(2),sind(3),:));
%Szv = squeeze(Sz(sind(1),sind(2),sind(3),:));
%S = conj(Sxv(:))*Sxv(:).' + conj(Syv(:))*Syv(:).' + conj(Szv(:))*Szv(:).';

    


