function totalScore = getMultibandScore(tse,diff)

% Input: tse and diff structs with fields:
%   rf: rf waveform (length-Nt vector)
%   g: gradient waveform (length-Nt vector)
%   dt: dwell time (scalar, seconds)

% get the tse evaluation parameters
tseParams;

% check that the fields we need are in the structure
if ~isfield(tse,'rf') || ~isfield(tse,'g') || ~isfield(tse,'dt')
    error 'tse structure is missing one or more required fields'
end

% evaluate the tse pulse
[tseIsValid,tseDur,tseErrorCode] = multibandEval(tse.rf,tse.g,tse.dt,evalp);

% get the diffusion evaluation parameters
clear evalp;
diffParams;

% check that the fields we need are in the structure
if ~isfield(diff,'rf') || ~isfield(diff,'g') || ~isfield(diff,'dt')
    error 'diff structure is missing one or more required fields'
end

% evaluate the diffusion pulse
[diffIsValid,diffDur,diffErrorCode] = multibandEval(diff.rf,diff.g,diff.dt,evalp);

% calculate total score as sum of two pulse durations (us)
if tseIsValid && diffIsValid
    totalScore = tseDur + diffDur; 
else
    totalScore = Inf;
end
