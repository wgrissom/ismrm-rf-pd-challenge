% Function to zip up all the example files for the multiband
% RF Pulse Design Challenge

fileList = ...
    {'dotrap.m',...
    'dz_pins.m',...
    'singleSliceRF.mat',...
    'PINSRFandGrad.mat',...
    'multibandExamples.m',...
    '../tseParams.m',...
    '../diffParams.m',...
    '../multibandEval.m',...
    '../blochsim.m',...
    '../gen_mb_eval_prof.m',...
    '../MBCandidate.mat'};

archiveName = 'multibandExamples.zip';

command = ['zip ' archiveName];
for ii = 1:length(fileList)
    command = [command ' ' fileList{ii}];
end

unix(command);

% move it to Dropbox
destFolder = '~/Documents/Dropbox/ismrmrfpd/exampleArchives/';
command = ['mv ' archiveName ' ' destFolder];
unix(command);

% also copy the walkthrough pdf to Dropbox
walkthroughName = 'MBChallengeCodeWalkthrough.pdf';
command = ['cp ' walkthroughName ' ' destFolder];
unix(command);

% also copy the example candidate file 
exampleMatName = '../MBCandidate.mat';
command = ['cp ' exampleMatName ' ' destFolder];
unix(command);


